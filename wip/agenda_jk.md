
NERD Stuff - topic ideas
========================

Introduction (2 minutes)
------------------------

Let's start with an introduction to Node.js, especially what Node.js is not. It is NOT a server running in the background waiting for applications to be deployed and running many of them like it is in the case of Java application servers.

Node.js is a runtime which runs your JavaScript code and when that code finishes, the Node.js process terminates.

If your code opens a network port and handles incoming requests, you have a server - like in the example below.

const http = require('http');

const server =  http.createServer((req, res) => {    
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.write(new Date().toISOString());
    res.end();
});

server.listen({port:8080}, () => {
    console.log("listening")
});

 When you want to run multiple web applications on the same machine, you will run multiple node.js processes, each opening its own port.

Web applications are more complex than this example - they need logging, authentication, session and cookies management and more. There are frameworks that help with this. One of the most used is the Express framework. It basically lets you to define endpoints and you write code that runs when a request to that endpoint is received. Response to these requests is built dynamically usually loading data from a datastore like a relational or document oriented database. And this is where the Domino AppDev Pack comes in.




Proton, Design Catalog, DQL
---------------------------
[image 01_protonschema.jpg]

For the dominoDB module to work, there are few things needed on the server side. The Proton task must be installed on the server. You simply copy one file to the Domino program directory and set configuration parameters in notes.ini [show slide with the config]

The Proton task can encrypt the communication and it also operates in either Anonymous or Authenticated mode. We will talk about that later.

Domino AppDev Pack should be released every 3 months. When upgrading, upgrade the Proton task first - older versions of dominoDB module should be able to talk to a newer Proton task.

The dominoDB module loads collection of data using the Domino Query Language facility, which is new in Domino 10. There is no other way, you can't walk through a view entry by entry. You need to run a DQL query on a database. To be able to do that, the database needs to be added to a design catalog. Use server console command load updall \<database path\> -e to add a database to the catalog and a -d flag to update it when the design changes.

We will not through the syntax here, it is described in the documentation. let's focused on few important things:
- DQL scans only a limited number of documents and view entries (200 000 by default). If you need more, set the limit using the QUERY_MAX_DOCS_SCANNED and QUERY_MAX_VIEW_ENTRIES_SCANNED notes.ini settings. There is also a limit how long the query will run (2 minutes by default). Increase that using the QUERY_MAX_MSECS_TOTAL notes.ini setting (in milliseconds).
- DQL returns the data sorted by NoteID, you can't have it sorted (yet - sorting should be available in Domino 11) and it also returns only a subset of the results. So to present data to users in meaningfull way, you have to load all of it and sort it yourself.
- Be aware that the DQL syntax in "space aware" - you need to put spaces between operators and values and item names.
- Use the Explain feature to analyze your queries and optimize them 




CRUD
----
Slide 1:
The domino-db module has only 3 classes - Server, Database and Document. Before you can work with documents, you need to connect to a Database. Before you can do that, you
need to use the Server.

Slide 2:
In a sample it looks something like this:
- there is one entry point - the useServer function which needs a configuration object. You can then open a database and work with the documents.

In real application, the configuration parameter will be read from the environment and the server and the database objects will be cached and reused. We have provided a demo application.

CRUD Tips
---------
Call useServer() and useDatabase() only once and cache the objects in DAO object
Separate all domino-db operations to a DAO object for better structured app


CRUD Tips - Create
------------------
- When creating a document, you need to provide the "Form" property
- When writing a Date item, you need to specify it like this:
   DueDate: { type: 'datetime', data: '2019-06-30'}
- Time needs to be specified to a hundreth of a second, you can't directly use a javascript Date object
- You can specify computeOptions to recompute the document with form

CRUD Tips - Read
------------------
- Get a single document by Universal ID or a collection by running a DQL query
- in either case get onlz the information you need by specifying required item names
- use compute optons parameter to compute the content of computed for display items

CRUD Tips - Update
------------------
- Lot of options for updating documents
  - update selected items in one or more documents at once
  - replace all items with a set of other items
- documents are immediately saved, there is no save() method
- when updating collection of documents, no data is sent back and forth - all is done on the server.


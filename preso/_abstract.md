# EngageUG 2019 Session Abstract

Title

## The NERD stuff: opening Domino to the modern web developer

![nerdherd](images/nerdherd.jpg)

Speaker

### Jan Krejcarek, PPF banka

![Jan](images/jan.jpg)

### Oliver Busse, We4IT

![Oliver](images/oliver.png)

Abstract

> With web development in Node.js getting more and more popular, IBM opened the Domino server to the Javascript stack. It's never too late to attract people outside the yellow bubble and to encourage Notes developers to learn new stuff. The session covers the general idea of using the full NERD stack (Node, Express, React and Domino), the components used in IBM Domino, security strategies and of course a lot of (live) coding that shows, how easy it is to open up a new world. Domino is a great database environment and now you can even use it with the sexy stuff all the young and fancy people use for years. We are here to prove that even the "traditional" Notes guys can make it!


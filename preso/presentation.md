<!-- $theme: default -->

<style>
table {
  border: none;
}
td {
  vertical-align: top;
  text-align:center;
  border: none;
}

ul {
  font-size: 0.9em;
}

code {
  font-size: 0.7em!important;
}

.engageLogo {
	position:fixed;
    top:20px;
    right:20px;
    width:100px
}
</style>

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

###### De07
# The NERD stuff: opening Domino to the modern web developer

<table>
  <tr>
    <td width="500px">
      <div><img src="images/jan.jpg" style="height:200px"/></div>
      <div>Jan Krejcarek</div>
      <div>PPF banka (CZ)</div>
      <div>@jan_krejcarek</div>
      <img src="images/ibmchampion.jpg"/>
    </td>
    <td width="500px">
      <div><img src="images/oliver.png" style="height:200px"/></div>
      <div>Oliver Busse</div>
      <div>We4IT (D)</div>
      <div>@zeromancer1972</div>
      <div></div>
    </td>
  </tr>
</table>

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Agenda

###### Spoiler: NERD = Node.js, Express, React, Domino

- Node.js introduction
- AppDev Pack
- Proton, Design Catalog &amp; DQL
- Development, Testing, Deployment
- CRUD demo
- Security options
- Getting Help
- Resume: why?
- Q &amp; A

<!-- footer: Engage 2019 - The NERD stuff: opening Domino to the modern web developer (De07)-->

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Node.js introduction

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Node.js is NOT a server
- But you can code one

---
# Simplest server example

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

<pre style="color:#000000;background:#ffffff;"><span style="color:#800000; font-weight:bold; ">const</span> http <span style="color:#808030; ">=</span> require<span style="color:#808030; ">(</span><span style="color:#800000; ">'</span><span style="color:#0000e6; ">http</span><span style="color:#800000; ">'</span><span style="color:#808030; ">)</span><span style="color:#800080; ">;</span>

<span style="color:#800000; font-weight:bold; ">const</span> server <span style="color:#808030; ">=</span> http<span style="color:#808030; ">.</span>createServer<span style="color:#808030; ">(</span><span style="color:#808030; ">(</span>req<span style="color:#808030; ">,</span> res<span style="color:#808030; ">)</span> <span style="color:#808030; ">=</span><span style="color:#808030; ">&gt;</span> <span style="color:#800080; ">{</span>    
    res<span style="color:#808030; ">.</span>statusCode <span style="color:#808030; ">=</span> <span style="color:#008c00; ">200</span><span style="color:#800080; ">;</span>
    res<span style="color:#808030; ">.</span>setHeader<span style="color:#808030; ">(</span><span style="color:#800000; ">'</span><span style="color:#0000e6; ">Content-Type</span><span style="color:#800000; ">'</span><span style="color:#808030; ">,</span> <span style="color:#800000; ">'</span><span style="color:#0000e6; ">text/plain</span><span style="color:#800000; ">'</span><span style="color:#808030; ">)</span><span style="color:#800080; ">;</span><br/>
    res<span style="color:#808030; ">.</span>write<span style="color:#808030; ">(</span><span style="color:#800000; font-weight:bold; ">new</span> <span style="color:#797997; ">Date</span><span style="color:#808030; ">(</span><span style="color:#808030; ">)</span><span style="color:#808030; ">.</span>toISOString<span style="color:#808030; ">(</span><span style="color:#808030; ">)</span><span style="color:#808030; ">)</span><span style="color:#800080; ">;</span><br/>
    res<span style="color:#808030; ">.</span>end<span style="color:#808030; ">(</span><span style="color:#808030; ">)</span><span style="color:#800080; ">;</span>
<span style="color:#800080; ">}</span><span style="color:#808030; ">)</span><span style="color:#800080; ">;</span>

server<span style="color:#808030; ">.</span>listen<span style="color:#808030; ">(</span><span style="color:#800080; ">{</span>port<span style="color:#800080; ">:</span><span style="color:#008c00; ">8080</span><span style="color:#800080; ">}</span><span style="color:#808030; ">,</span> <span style="color:#808030; ">(</span><span style="color:#808030; ">)</span> <span style="color:#808030; ">=</span><span style="color:#808030; ">&gt;</span> <span style="color:#800080; ">{</span>
    console<span style="color:#808030; ">.</span><span style="color:#800000; font-weight:bold; ">log</span><span style="color:#808030; ">(</span><span style="color:#800000; ">"</span><span style="color:#0000e6; ">listening</span><span style="color:#800000; ">"</span><span style="color:#808030; ">)</span>
<span style="color:#800080; ">}</span><span style="color:#808030; ">)</span><span style="color:#800080; ">;</span>
</pre>

---
<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Frameworks for Node.js web apps
- Express
- Koa

---
<img src="images/EngageLogo_Black.png" class="engageLogo"/>

 # Express basic principle
 
 <img src="images/urlendpoint.jpg"/>

Code for the endpoint:
<pre style="font-size:14pt; color:#000000;background:#ffffff;">router<span style="color:#808030; ">.</span>get<span style="color:#808030; ">(</span><span style="color:#800000; ">'</span><span style="color:#0000e6; ">/certificates/expiring/:someDate?</span><span style="color:#800000; ">'</span><span style="color:#808030; ">,</span> async <span style="color:#808030; ">(</span>req<span style="color:#808030; ">,</span> res<span style="color:#808030; ">,</span> next<span style="color:#808030; ">)</span> <span style="color:#808030; ">=</span><span style="color:#808030; ">&gt;</span> <span style="color:#800080; ">{</span>    
    <span style="color:#800000; font-weight:bold; ">let</span> d <span style="color:#808030; ">=</span> <span style="color:#0f4d75; ">null</span><span style="color:#800080; ">;</span>
    <span style="color:#800000; font-weight:bold; ">if</span> <span style="color:#808030; ">(</span><span style="color:#800000; font-weight:bold; ">typeof</span> req<span style="color:#808030; ">.</span>params<span style="color:#808030; ">.</span>someDate <span style="color:#808030; ">!=</span> <span style="color:#800000; ">'</span><span style="color:#0000e6; ">undefined</span><span style="color:#800000; ">'</span><span style="color:#808030; ">)</span> <span style="color:#800080; ">{</span>
        d <span style="color:#808030; ">=</span> <span style="color:#800000; font-weight:bold; ">parse</span><span style="color:#808030; ">(</span>req<span style="color:#808030; ">.</span>params<span style="color:#808030; ">.</span>someDate<span style="color:#808030; ">)</span><span style="color:#800080; ">;</span>
    <span style="color:#800080; ">}</span> <span style="color:#800000; font-weight:bold; ">else</span> <span style="color:#800080; ">{</span>
        d <span style="color:#808030; ">=</span> addMonths<span style="color:#808030; ">(</span>endOfDay<span style="color:#808030; ">(</span><span style="color:#800000; font-weight:bold; ">new</span> <span style="color:#797997; ">Date</span><span style="color:#808030; ">(</span><span style="color:#808030; ">)</span><span style="color:#808030; ">)</span><span style="color:#808030; ">,</span> <span style="color:#008c00; ">3</span><span style="color:#808030; ">)</span><span style="color:#800080; ">;</span>
    <span style="color:#800080; ">}</span>

    <span style="color:#800000; font-weight:bold; ">try</span> <span style="color:#800080; ">{</span>
        <span style="color:#800000; font-weight:bold; ">var</span> docs <span style="color:#808030; ">=</span> await dao<span style="color:#808030; ">.</span><span style="font-weight:bold">getCertificatesExpiringBefore</span><span style="color:#808030; ">(</span>d<span style="color:#808030; ">)</span><span style="color:#800080; ">;</span>
        res<span style="color:#808030; ">.</span>json<span style="color:#808030; ">(</span>docs<span style="color:#808030; ">)</span><span style="color:#800080; ">;</span>
    <span style="color:#800080; ">}</span> <span style="color:#800000; font-weight:bold; ">catch</span> <span style="color:#808030; ">(</span>error<span style="color:#808030; ">)</span> <span style="color:#800080; ">{</span>
        res<span style="color:#808030; ">.</span>status<span style="color:#808030; ">(</span><span style="color:#008c00; ">500</span><span style="color:#808030; ">)</span><span style="color:#808030; ">.</span>json<span style="color:#808030; ">(</span><span style="color:#800080; ">{</span>error<span style="color:#800080; ">:</span>error<span style="color:#808030; ">.</span>message<span style="color:#800080; ">}</span><span style="color:#808030; ">)</span>
    <span style="color:#800080; ">}</span>
<span style="color:#800080; ">}</span><span style="color:#808030; ">)</span>
</pre>

---


<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Domino AppDev Pack
- Standalone distribution  (part number CC0NGEN)
- Not part of the Notes/Domino installation package
- Not supported in Domino Designer (which is good)
- Available for Domino on Linux and Windows (since v1.0.1, March 26, 2019)
- Quarterly releases

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Content of the AppDev Pack
- Proton server task
- DominoDB module for Node.js (not available on npmjs.org, yet)
- Demo application with data
- Documentation
  - Installation procedure
  - API documentation
  - Domino Query Language syntax
  - IAM examples

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Proton schema
![Proton schema](images/01_protonschema.jpg)

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Proton task installation
- Copy the "proton" file to the Domino program directory
- Add "Proton" to the ServerTasks variable in notes.ini to start the Proton task when Domino starts
- Configure the Proton using notes.ini variables:
	- PROTON_LISTEN_ADDRESS=0.0.0.0
		- Listens on all available network interfaces
	- PROTON_LISTEN_PORT=30000
		- TCP/IP port to use
	- PROTON_SSL=1
		- Use TLS for communication
	- PROTON_KEYFILE=server.kyr
		- Can be the same Domino already uses for TLS
	- PROTON_AUTHENTICATION=client_cert
		- Authenticate Node.js apps using a certificate

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Recommended setting for production
- Encrypt the communication
- Authenticate applications using a certificate

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Three-tier architecture
- By moving application logic to Node.js and the presentation logic to the browser we are actually moving to a three-tier architecture where Domino functions as a storage
![Layers](images/02_layers.jpg)

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Proton
# Three-tier architecture
- In that case users will be authenticated by the application layer (Node.js) and the Node.js application will use another account to access the data
![Users](images/03_users.jpg)
	
---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Proton
# How it works
- Proton sees Node.js application connecting using DominoDB module as a user with certain rights
- All operations on data are executed under the authority of this user
- This users needs sufficient rights in the ACL of the database it uses
- It is the Node.js application's task to ensure correct access to data to its users

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Proton
# Authenticating using a certificate
- Node.js application using DominoDB needs to have a Person document in the Domino Directory
- It also needs an X.509 certificate and hold the private key to that certificate
- The certificate needs to be loaded to the Person document
	- Import Internet Certificate action on the Person document
- The name in the Person document must correspond to the subject's common name from the certificate
- The user needs to have access to the database via ACL

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Person document for the application
![Person document for the app](images/04_persondocument.jpg)

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# ACL record in the database
![ACL record for the app](images/05_acl.jpg)

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Running Proton
- command on the Domino server console: load proton
![Running Proton](images/06_runningproton.jpg)

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Design Catalog, DQL

- domino-db module loads a collection of documents by running a DQL query. 
- Design Catalog is needed for DQL to work properly. 

---
<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Design Catalog, DQL


Add a database to the catalog:
- `load updall <database path> -e`

Update a database in the catalog when the design changes:
- `load updall <database path> -d`

When the update fails:
<img src="images/UpdallError.jpg"/>

run the updall again with the -e flag:
<img src="images/UpdallSuccess.jpg"/>

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# DQL Facts 1/3

- DQL scans a limited number of documents and view entries (200 000)
  - increase it with a notes.ini setting (system wide)
    - QUERY_MAX_DOCS_SCANNED
    - QUERY_MAX_VIEW_ENTRIES_SCANNED
- DQL query runs for a limited time (2 minutes)
  - rethink your design

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# DQL Facts 2/3
- Proton returns the results sorted by NoteID and returns only a subset of the results (max 200).
  - You have to load all results and sort them yourself
  - Sorting should be available in Domino 11

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# DQL Facts 3/3
- "Mind the gap"
  - DQL query needs spaces around operators, values and item names
    - `'Cards'.Subtypes ='Beast'` 
    - ERR_BAD_REQUEST: Query is not understandable -  syntax error    - MUST have at least one operator
- Use the explainQuery() method to analyze your queries and optimize them 

```Query Processed:
['Cards'.Subtypes = 'Beast' AND 'Cards'.ConvertedManaCost > 4]

0. AND           (childct 2) (totals when complete:) Prep 0.0 msecs, Exec 71.428 msecs, ScannedDocs 0, Entries 4730, FoundDocs 162
        1.'Cards'.Subtypes = 'Beast' View Column Search estimated cost = 5
                Prep 0.326 msecs, Exec 3.506 msecs, ScannedDocs 0, Entries 248, FoundDocs 248
        1.'Cards'.ConvertedManaCost > 4 View Column Search estimated cost = 10
                Prep 0.112 msecs, Exec 67.915 msecs, ScannedDocs 0, Entries 4482, FoundDocs 162
```

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Setting up your development environment (1)

- Install Node.js runtime (Win, Mac, Linux)
- Grab your favorite editor (VS Code recommended)
- Init the project
	```
    mkdir myProject
    cd myProject
    npm init
    ```
- Add the domino-db Node package to your project
	- `npm install <pathToAppDevPack>/domino-domino-db-1.2.0.tgz -save`
- Start coding

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Setting up your development environment (2)

- Check the package.json file
	- it will already contain the depency for domino/domino-db
	- add other dependencies
- Create the start javascript file

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Testing? Sure!

- Several unit test packages available
	- Mocha
	- Tape
	- Chai
	- Sinon
- Tests are defined in separate files
- Tests are configured in the package.json file
```
{
  "test-unit": "NODE_ENV=test mocha '/**/*.spec.js'",
}
```

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Deployment (1)

- Scenarios to consider
	- deploy to a cloud service like AWS, Azure, IBM Cloud, Heroku
	- deploy to on-premises environment

- On-premises
	- Does your server have internet access to install packages?
	- Does your server utilize a load balancer?
	- Hot deployment or not?
	- Using a Docker container?
	- Using a proxy like nginx

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Deployment - using a proxy

- As Node.js apps can use different ports you should utilize them with a proxy to keep the URL endpoints simple
- nginx is a lightweight proxy server which is easy to set up

```
# excerpt from nginx.conf
location /app1 {
    proxy_pass http://localhost:3001/;
    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
}

location /app2 {
    proxy_pass http://localhost:3002/;
    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
}
```

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Deployment - Process Manager (1)

- Use a process manager to handle all your apps
- ***Do not*** start your apps manually or with a systemd script/service
- pm2 is highly recommended
- Very simple to set up

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Deployment - Process Manager (2)

- start the app with the process manager

```
pm2 start myApp.js
```

- show all apps managed by pm2

```
pm2 ls

┌───────────────────────┬────┬─────────┬──────┬───────┬────────┬─────────┬────────┬──────┬────────────┬──────┬──────────┐
│ App name              │ id │ version │ mode │ pid   │ status │ restart │ uptime │ cpu  │ mem        │ user │ watching │
├───────────────────────┼────┼─────────┼──────┼───────┼────────┼─────────┼────────┼──────┼────────────┼──────┼──────────┤
│ node alexa-node-red   │ 11 │ N/A     │ fork │ 24488 │ online │ 0       │ 2M     │ 0%   │ 16.5 MB    │ root │ disabled │
│ node domino-node-list │ 9  │ N/A     │ fork │ 24152 │ online │ 0       │ 2M     │ 0%   │ 55.5 MB    │ root │ disabled │
│ node rootweb          │ 10 │ N/A     │ fork │ 24229 │ online │ 0       │ 2M     │ 0%   │ 18.9 MB    │ root │ disabled │
│ node-red              │ 0  │ N/A     │ fork │ 5463  │ online │ 6       │ 49D    │ 0.3% │ 154.6 MB   │ root │ disabled │
└───────────────────────┴────┴─────────┴──────┴───────┴────────┴─────────┴────────┴──────┴────────────┴──────┴──────────┘

```
---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Deployment - Process Manager (3)

- save the apps to the list of bootable apps

```
pm2 save
```

- enable pm2 to start all saved apps at boot
- 
```
pm2 startup
```

All commands are the same on Win, Mac &amp; Linux

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# domino-db module

---
<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# domino-db classes
![DominoDB API](images/07_api.jpg)

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# domino-db module
- provides four basic operations with data - Create, Read, Update, Delete  
- has a single entry point: userServer() function
```
const { useServer } = require('@domino/domino-db');
const server = await useServer({hostName: 'localhost', connection:{ port: '30000' }});
```

---
<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# domino-db command line example


<pre style="font-size: 70%;color:#000000;background:#ffffff;"><span style="color:#800000; font-weight:bold; ">const</span> <span style="color:#800080; ">{</span> useServer <span style="color:#800080; ">}</span> <span style="color:#808030; ">=</span> require<span style="color:#808030; ">(</span><span style="color:#800000; ">'</span><span style="color:#0000e6; ">@domino/domino-db</span><span style="color:#800000; ">'</span><span style="color:#808030; ">)</span><span style="color:#800080; ">;</span>

<span style="color:#800000; font-weight:bold; ">const</span> serverConfig <span style="color:#808030; ">=</span> <span style="color:#800080; ">{</span>hostName<span style="color:#800080; ">:</span> <span style="color:#800000; ">'</span><span style="color:#0000e6; ">localhost</span><span style="color:#800000; ">'</span><span style="color:#808030; ">,</span>connection<span style="color:#800080; ">:</span><span style="color:#800080; ">{</span> port<span style="color:#800080; ">:</span> <span style="color:#800000; ">'</span><span style="color:#0000e6; ">30000</span><span style="color:#800000; ">'</span> <span style="color:#800080; ">}</span><span style="color:#800080; ">}</span><span style="color:#800080; ">;</span>
<span style="color:#800000; font-weight:bold; ">const</span> databaseConfig <span style="color:#808030; ">=</span> <span style="color:#800080; ">{</span> filePath<span style="color:#800080; ">:</span> <span style="color:#800000; ">'</span><span style="color:#0000e6; ">database/node-demo.nsf</span><span style="color:#800000; ">'</span> <span style="color:#800080; ">}</span><span style="color:#800080; ">;</span>

<span style="color:#808030; ">(</span>async <span style="color:#800000; font-weight:bold; ">function</span><span style="color:#808030; ">(</span><span style="color:#808030; ">)</span> <span style="color:#800080; ">{</span>
    <span style="color:#800000; font-weight:bold; ">try</span> <span style="color:#800080; ">{</span>
        <span style="color:#800000; font-weight:bold; ">const</span> server <span style="color:#808030; ">=</span> await useServer<span style="color:#808030; ">(</span>serverConfig<span style="color:#808030; ">)</span><span style="color:#800080; ">;</span>
        <span style="color:#800000; font-weight:bold; ">const</span> db <span style="color:#808030; ">=</span> await server<span style="color:#808030; ">.</span>useDatabase<span style="color:#808030; ">(</span>databaseConfig<span style="color:#808030; ">)</span><span style="color:#800080; ">;</span>

        <span style="color:#800000; font-weight:bold; ">const</span> response <span style="color:#808030; ">=</span> await db<span style="color:#808030; ">.</span>bulkReadDocuments<span style="color:#808030; ">(</span><span style="color:#800080; ">{</span>
            query<span style="color:#800080; ">:</span> <span style="color:#800000; ">"</span><span style="color:#0000e6; ">'AllContacts'.State = 'FL'</span><span style="color:#800000; ">"</span><span style="color:#808030; ">,</span>
            itemNames<span style="color:#800080; ">:</span> <span style="color:#808030; ">[</span><span style="color:#800000; ">'</span><span style="color:#0000e6; ">LastFirstName</span><span style="color:#800000; ">'</span><span style="color:#808030; ">,</span> <span style="color:#800000; ">'</span><span style="color:#0000e6; ">Email</span><span style="color:#800000; ">'</span><span style="color:#808030; ">]</span><span style="color:#808030; ">,</span>
            computeOptions<span style="color:#800080; ">:</span> <span style="color:#800080; ">{</span> computeWithForm<span style="color:#800080; ">:</span> <span style="color:#0f4d75; ">true</span> <span style="color:#800080; ">}</span>
        <span style="color:#800080; ">}</span><span style="color:#808030; ">)</span><span style="color:#800080; ">;</span>
        console<span style="color:#808030; ">.</span><span style="color:#800000; font-weight:bold; ">log</span><span style="color:#808030; ">(</span><span style="color:#797997; ">JSON</span><span style="color:#808030; ">.</span><span style="color:#800000; font-weight:bold; ">stringify</span><span style="color:#808030; ">(</span>response<span style="color:#808030; ">)</span><span style="color:#808030; ">)</span><span style="color:#800080; ">;</span>
    <span style="color:#800080; ">}</span> <span style="color:#800000; font-weight:bold; ">catch</span> <span style="color:#808030; ">(</span>error<span style="color:#808030; ">)</span> <span style="color:#800080; ">{</span>
        console<span style="color:#808030; ">.</span><span style="color:#800000; font-weight:bold; ">log</span><span style="color:#808030; ">(</span><span style="color:#800000; ">`</span><span style="color:#800000; ">${</span><span style="color:#797997; ">error</span><span style="color:#808030; ">.</span><span style="color:#797997; ">code</span><span style="color:#800000; ">}</span><span style="color:#0000e6; ">: </span><span style="color:#800000; ">${</span><span style="color:#797997; ">error</span><span style="color:#808030; ">.</span><span style="color:#797997; ">message</span><span style="color:#800000; ">}</span><span style="color:#800000; ">`</span><span style="color:#808030; ">)</span>
    <span style="color:#800080; ">}</span>
<span style="color:#800080; ">}</span><span style="color:#808030; ">)</span><span style="color:#808030; ">(</span><span style="color:#808030; ">)</span>
</pre>


---


<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# DEMO - Web Application

---
<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# CRUD Details

- Call useServer() and Server::useDatabase() only once and cache the instances in a custom Data Access Object

---
<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# CRUD Details - Create
- When creating a document, you need to provide the "Form" property
- When writing a Date item, you need to specify it like this:
   `DueDate: { type: 'datetime', data: '2019-06-30'}`
- Time needs to be specified to a hundredth of a second, you can't directly use a JavaScript Date object
- You can specify computeOptions to compute items in the document

---
<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# CRUD Details - Read
- You always specify which items you want to receive (keeps the amount of transfered data low)
- use computeOptions parameter to compute the content of computed for display items

```
const response = await db.bulkReadDocuments({
  query: "'AllContacts'.State = 'FL'",
  itemNames: ['LastFirstName', 'Email'],
  computeOptions: { computeWithForm: true }
});
```

---
<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# CRUD Details - Update
- Selection of options for updating documents
  - change selected items in one or more documents at once
  - replace all items with a set of other items
- Documents are immediately saved, there is no save() method
---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Security Options (1)

- IAM (Identity Access Management) enables access from a Node.js app to Domino as a real DNN user
- Uses OAuth mechanism to authorize against Domino
- Domino 10 provides OAuth (kind of)
- Access is restricted to certain scopes
	- open_id
	- offline_access
	- das.freebusy
	- das.calendar.read.with.shared
	- das.calendar.write.with.shared
- Currently IAM does not support the Node.js API for Domino, only DAS :-(

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Security Options (2)

- Setup Domino as an OAuth provider is time consuming (expect 1 work day)
- You will need to touch the following areas of (Domino) administration
	- Certificate handling
	- Keyring generation
	- Domino with SSL
	- Domino LDAP configuration
	- OAuth DSAPI setup
	- Setup the IAM service app (server part)
	- Customize the IAM service app (optional)

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Security Options (3)

- IAM comes with examples of different authorization flows
	- Authorization Code Flow
	- Client Credential Flow
- These examples can be used to integrate those mechanism to your own app (i.e. IAM client app)
- Apps must be registered with the IAM service app (generates the app ID and the app secret)
- ***You*** are responsible for the token handling when accessing the Domino server!

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Getting Help / Resume: why?

- NERD is completely different from traditional Notes development
- Getting help
	- Domino with Node is discussed in the OpenNTF Slack channel *dominonodejs*
	- Reach out for the experts like Dan Dumont, Pei Sun or Heiko Voigt (all active on Slack)
- Why you should use it?
	- It opens Domino for the modern web developer (new blood for the best app server in the world)
	- Offers tons of new possibilities coming with various Node modules and plugins
	- Using e.g. Node-RED directs to the real low-code area - but this is content for separate sessions ;-)

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Q &amp; A

<img src="images/qa.png"/>

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

# Resources
<ul style="font-size:0.5em">
<li>Webinar DQL &amp; FAQ: https://www.ibm.com/blogs/collaboration-solutions/2019/02/04/domino-query-language-faq</li>
<li>Domino AppDevPack Documentation: https://doc.cwpcollaboration.com/appdevpack/docs/en/homepage.html</li>
<li>Visual Studio Code Editor: https://code.visualstudio.com/</li>
<li>Unit testing and TDD in Node.js: https://www.codementor.io/davidtang/unit-testing-and-tdd-in-node-js-part-1-8t714s877</li>
<li>Node.js Unit Testing: https://blog.risingstack.com/node-hero-node-js-unit-testing-tutorial/</li>
<li>Nginx web server: https://nginx.org/en/docs/</li>
<li>pm2 process manager: https://pm2.io/runtime/</li>
<li>Stack Overflow Node.js: https://stackoverflow.com/questions/tagged/node.js</li>
<li>OpenNTF Slack: https://slackin.openntf.org/</li>
<li>Node-RED: https://nodered.org/</li>
<li>Session Repo: https://gitlab.com/obusse/engage-2019</li>
</ul>

---

<img src="images/EngageLogo_Black.png" class="engageLogo"/>

<img src="images/thanks.gif"/>

---
# Topic ideas for agenda

- intro to web development in general

  The increasing popularity and impact of Node.js. The difference in building apps with Node.js - we build REST API endpoints that produce and consume JSON data. The data lies on different server in some other environment (MongoDB, PostgreSQL, Domino) as opposed to XPages where data and the app is in the same environment (Domino) and we have a rich API to use it. (for many people the Node.js will be new, so I think we should explain this).

  To get to the data in Domino environment from Node.js, we use the new domino-db module.
  Mention the deployment model for Node.js apps (as You mentioned in the Beta forum - a container is spawned, git project from repository downloaded, set up and launched with Node.js)

- intro to domino-db

  what it is (a bunch of JavaScript files?), where to get it, how it communicates with the Domino server.

- the new Proton task

  Why there is a new task, how it communicates, what is gRPC and why it was chosen over some other methods.
  How to setup Proton for production (using SSL/TLS and client certification authentication.
  
  (I hope that the Proton task will be a part of Domino by that time, so we will not cover the installation).

- explain the idea behind a certificate for each app

  ...which is possibily NOT the way to go, instead we also should explain the IAM stuff - VERY SHORTLY of course
  Setup of IAM will be simplified with upcoming releases, so this is the way to go
  I will explain the IAM basics like OAuth and how to set it up in general (just bullets, no details)


- using the domino-db module

  - catalog the app for DQL
  - the entry point (useServer), the configuration object
  - opening a database
  - reading many documents
  - reading a single document
  - creating a document
  - updating a document
  - deleting a document
  - working with date and time
  - working with multivalue items

Would be great to live code the main parts - useServer, open database, get documents, create a document and to show the results. Then load a branch from a repo and show the code for other usages.

- setting up a dev environment

  Editor of choice, git & stuff

- Getting Help

  I added this section as this is essential to start as documentation is rarely provided. Mention of Slack channel(s), blogs and persons (via Slack), some of them will be on site, too (e.g. Dan Dumont, Heiko Voigt)